# Pitiki

A constructed language based on simple lines and arcs.

The font is released under the [SIL Open Font License](https://scripts.sil.org/OFL)
